# Feature 2

[x] Create superuser

# Feature 3

[x] Create model called Project in the "projects" app
[x] Create properties: name, description, members
[x] Makemigrations and migrate

# Feature 4

[x] Add "Project" model in the admin.py

# Feature 5

[x] Create urls.py in the "projects" app
[x] Use "" as path and the name "list_projects" in the app urls.py file
[x] Create ProjectListView
[x] Create "templates" folder and create list.html
[x] Create urlpatterns in "tracker" project urls.py to include the "project" app urls

# Feature 6

[x] Create a redirect page in urls.py in the "tracker" to ProjectListView page

# Feature 7

[x] Import LoginView in accounts/urls.py with path "login/" and name "login"
[x] Include the URL patterns from accounts app in "tracker" using "accounts/"
[x] Create tempaltes folder under accounts
[x] Create registration folder under templates
[x] Create login.html in registration folder
[x] Use <form method ="post"> in the login.html with required HTML5 tags
[x] Go "tracker" settings.py, create and add LOGIN_REDIRECT_URL to "home"

# Feature 8

[x] protect the ProjectListView with LoginRequiredMixin
[x] overwrite get_querset to filter the Project objects where members = user

# Feature 9

[x] In accounts/urls.py, import LogoutView with path "logout/" and name "logout"
[x] Go "tracker" settings.py, create and add LOGOUT_REDIRECT_URL to "login"

# Feature 10

[x] Import UserCreationForm to the accounts/views.py
[x] Use create_user method to create user account
[x] Use the login function that logs account in
[x] Create signup.html in teh registration folder
[x] Create <form method="post"> in the signup.html with all needed HTML tags and token, form.as_p

# Feature 11

[x] Create Task model in "tasks" app
[x] Migrations and migrate

# Feature 12

[x] Add the Task model to the admin.py

# Feature 13 (ProjectDetailView)

[x] Create a detail view of the "project" in projects app
[x] Use LoginRequiredMixin
[x] In urls.py, add path "<int:pk>/" with name of "show_project"
[x] Create detail.html template of project and a table tag with its tasks
[x] Update the list template to show number of tasks for the project
[x] Update the list template to have an a tag link to direct the page to detail view of project

# Feature 14 (ProjectCreateView)

[x] Create ProjectCreateView w/name, description, members
[x] LoginRequiredMixin
[x] Redirect to detail page once created the project
[x] Add path "create/" in the projects urls.py with name "create_project"
[x] Add create.html in the templates folder
[x] Add in <form method = "post>
[x] Add <a> tag "Create a new project" in the ProjuectListView page: after h1 tag but before table, should be inside div

# Feature 15 (TaskCreateView)

[x] Create TaskCreateView with name, start_date, due_date, project, assignee
[x] Login Mixin
[x] Redirect back to detail page of the task's project
[x] Add "create/" with name "create_task" in urls.py
[x] Add "tasks/" to urls.py for project url to include
[x] Create create.html template
[x] Add link of "Create a task" from project detail page
[x] Create all needed HTML and the form tag
[x] Add a tag of "Create a task" in side the detail.html of "project"

# Feature 16 (Show My Tasks list view)

[x] Create TaskListView
[x] Filter the TaskListView using get_querset so only see tasks assigned (assignee = current user)
[x] LoginRequiredMixin
[x] Add path "mine/" in the "tasks" app with name "show_my_tasks" in urls.py
[x] Create tasks_list.html with all needed HTML requirements
[x] If taks is True, show "Done"

# Feature 17 (completing a task)

[x] Create TaskUpdateView with only is_completed field
[x] Redirect to "show_my_tasks" (direct to the My Task view)
[x] Add "<int:pk>/complete/" with name of "complete_task" in tasks.urls.py
[x] Do not need HTML tempplate for this
[] Modift the My Tasks view to adjust the HTML need

# Feature 18 (markdownify)

[x] Install markdownify using pip install django-markdownify
[x] Add it to INSTALL_APPS in the settings.py of tracker project folder
[x] Add config to disable sanitation inside the settings.py file
[x] Load markdownify in the detail.html inside project app
[x] Replace p tag and {{project.description}} in the detail view HTML template in project folder

# Feature 19 (navigation)

[x] Follow instruction to add navigations
