from django.urls import path

from tasks.views import TaskCreateView, TaskListView, TaskUpdateView, task_search

urlpatterns = [
    path("create/", TaskCreateView.as_view(), name="create_task"),
    path("mine/", TaskListView.as_view(), name="show_my_tasks"),
    path("<int:pk>/complete/", TaskUpdateView.as_view(), name="complete_task"),
    path("search/", task_search, name="search"),
    # path("<int:pk>/edit/", TaskUpdateView.as_view(), name="update_task"),
]
