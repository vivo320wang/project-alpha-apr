from tasks.models import Task
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.shortcuts import render


def task_search(request):
    if request.method == "POST":
        searched = request.POST.get('searched')
        tasks = Task.objects.filter(name__icontains=searched)
        return render(request, "search.html", {'searched':searched, 'tasks':tasks})
    else:
        return render(request, "search.html", {})
    

class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    fields = ["name", "start_date", "due_date", "project", "assignee", "notes"]
    template_name = "tasks_create.html"

    def get_success_url(self):
        return reverse_lazy(
            "show_project", args=[self.object.project.pk]
        )
        return reverse_lazy(
            "show_project", kwargs={"pk": self.object.project.pk}
        )

class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks_list.html"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    fields = ["name", "due_date", "is_completed", "notes"]
    template_name = "tasks_update.html"
    success_url = reverse_lazy("show_my_tasks")


# Create your views here.
