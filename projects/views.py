from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from projects.models import Project
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

# from django.shortcuts import render, redirect


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "list.html"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "detail.html"


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    fields = ["name", "description", "members"]
    template_name = "create.html"

    def get_success_url(self):
        # return reverse_lazy("show_project", kwargs={"pk": self.object.pk})
        return reverse_lazy("show_project", args=[self.object.id])

# Create your views here.
